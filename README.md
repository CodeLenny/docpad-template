# DocPad Template

This is a basic DocPad skeleton.

## Technologies

This site uses the following software and technologies.
A quick description of each package is provided below.

### [DocPad][] (site generator)

[DocPad][] is used to assemble this website, supporting:

- Defining a build pipeline, customizing how the site is assembled
- Merging page contents (the text displayed) with a layout template
- Multiple markup languages (e.g. Markdown, AsciiDoc) so editors don't need to learn HTML

([All DocPad features][docpad-features])

The DocPad [configuration][docpad-configuration] is inside [`docpad.js`](./docpad.js).

The installed DocPad [plugins][docpad-plugins] are listed inside [`package.json`](./package.json).

### [Polymer][] (custom elements)

[Polymer][] allows custom HTML elements to be defined,
allowing complex layouts to be abstracted away for designers.

For instance, the [`g-r-i-d`][] package allows side-by-side columns:

```html
<g-r-i-d min-column-width="600" max-columns="2">
  <div>
    This is the left column.
  </div>
  <div>
    This is the right column.
  </div>
</g-r-i-d>
```

Where the left/right columns are displayed next to each other if they fit
(each column has a minimum width of `600px`),
and vertically stacked otherwise.

Elements found on [WebComponents.org][]
can be installed by listing them in [`bower.json`](./bower.json),
and they will be installed into [`src/raw/components`](./src/raw/components/).

Custom elements can be defined in [`src/raw/elements`](./src/raw/elements).

### [Surge.sh][] (temporary web publishing)

[Surge][Surge.sh] is used to preview the website during development.

### [GitLab CI][] (build pipeline)

[GitLab CI][] is used to automatically build and deploy the website when changed.

The build configuration is in [`.gitlab-ci.yml`](./.gitlab-ci.yml).

[DocPad]: https://docpad.org/
[docpad-features]: https://docpad.org/docs/intro/#comparison-table
[docpad-configuration]: https://docpad.org/docs/config/
[docpad-plugins]: https://docpad.org/docs/plugins/
[Polymer]: https://www.polymer-project.org/
[`g-r-i-d`]: https://www.webcomponents.org/element/vguillou/g-r-i-d
[WebComponents.org]: https://www.webcomponents.org/
[Surge.sh]: https://surge.sh/
[GitLab CI]: https://about.gitlab.com/features/gitlab-ci-cd/
